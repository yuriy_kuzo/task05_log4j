package com.kuzo;
import org.apache.logging.log4j.*;

public class ExampleLog {
    private  static Logger logger = LogManager.getLogger(ExampleLog.class);

    public static void main(String[] args) {
        logger.trace("trace message ");
        logger.debug("debug message ");
        logger.info("info message ");
        logger.warn("warn message ");
        logger.error("error message ");
        logger.fatal("fatal message ");
    }
}
